﻿using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    public int damagePerShot = 10;
	public float timeBetweenBullets = 0.50f;
	public float range = 85f;
	public float gunLightIntensity = 1.0f;
	private float origGunLightIntensity = 1.0f;
	private int origDamagePerShot = 10;
	private float origTimeBetweenBullets = 0.50f;
	private float origRange = 100f;
	Animator anim;

    float timer;
    Ray shootRay;
    RaycastHit shootHit;
    int shootableMask;
    ParticleSystem gunParticles;
    LineRenderer gunLine;
    AudioSource gunAudio;
	PlayerAutoAim playerAutoAim;
	PlayerHealth playerHealth;

    Light gunLight;
    float effectsDisplayTime = 0.2f;
	bool isAttack;
	KeyCode shootKey;

    void Awake ()
    {
        shootableMask = LayerMask.GetMask ("Shootable");
        gunParticles = GetComponent<ParticleSystem> ();
        gunLine = GetComponent <LineRenderer> ();
        gunAudio = GetComponent<AudioSource> ();
        gunLight = GetComponent<Light> ();
		playerHealth = GetComponentInParent<PlayerHealth> ();
		anim = playerHealth.gameObject.GetComponentInChildren<Animator> ();
		playerAutoAim = playerHealth.GetComponentInChildren<PlayerAutoAim>();

		isAttack = false;

		damagePerShot = origDamagePerShot;
		timeBetweenBullets = origTimeBetweenBullets;
		gunLightIntensity = origGunLightIntensity;
		range = origRange;
	
		if (playerHealth.gameObject.name == "Player1") {
			shootKey = PlayerInput.FIRE_KEY;
		} else if (playerHealth.gameObject.name == "Player2") {
			shootKey = PlayerInput.FIRE_KEY2;
		}
			
	}
		
    void Update ()
    {
        timer += Time.deltaTime;
	
		if(Input.GetKey (shootKey) && timer >= timeBetweenBullets && Time.timeScale != 0){
            Shoot ();
        }
			
        if(timer >= timeBetweenBullets * effectsDisplayTime){
            DisableEffects ();
        }

		anim.SetBool ("isAttacking", isAttack);
    }
		
    public void DisableEffects ()
    {
		isAttack = false;
        gunLine.enabled = false;
        gunLight.enabled = false;
    }

    void Shoot ()
    {
        timer = 0f;
		isAttack = true;
        gunAudio.Play ();
		gunLight.intensity = gunLightIntensity;
        gunLight.enabled = true;

        gunParticles.Stop ();
        gunParticles.Play ();

        gunLine.enabled = true;
        gunLine.SetPosition (0, transform.position);

        shootRay.origin = transform.position;
        shootRay.direction = transform.forward;

		if (playerAutoAim.LockedOn) {
			Vector3 temp = playerAutoAim.TargetCurrent.transform.position;
			temp.y += 0.63f;
			shootRay.direction = temp - transform.position;
		}

        if(Physics.Raycast (shootRay, out shootHit, range, shootableMask))
        {
            EnemyHealth enemyHealth = shootHit.collider.GetComponent <EnemyHealth> ();
			RandomObjectScript objectScript = shootHit.collider.GetComponent <RandomObjectScript> ();
			if (enemyHealth != null) {
				enemyHealth.TakeDamage (damagePerShot, shootHit.point, playerHealth.gameObject);
			} else if (objectScript != null) {
				objectScript.TakeDamage (damagePerShot, shootHit.point, playerHealth.gameObject );
			}
            gunLine.SetPosition (1, shootHit.point);
        } else {
            gunLine.SetPosition (1, shootRay.origin + shootRay.direction * range);
        }
    }
}