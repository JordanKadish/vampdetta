﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
	public float speed = 4.5f;
	public float maxSpeed = 8f;
	Animator anim;
	Rigidbody playerRigidBody;
	public float rotationSpeed = 5.0f;
	public static Transform playerPosition;

	bool tapLeft;
	bool tapRight;
	float timerLeft;
	float timerRight;
	float timer;
	float timeDelay = 2.0f;

	void Awake()	
	{
		timer = 0.0f;
		tapLeft = false;
		tapRight = false;
		timerLeft = 0.0f;
		timerRight = 0.0f;

		anim = GetComponentInChildren<Animator> ();
		playerRigidBody = GetComponent<Rigidbody> ();
		playerPosition = transform;
		speed = maxSpeed;
	}

	void FixedUpdate()	
	{
		timer += Time.deltaTime;
		maxSpeed = speed;
		playerPosition = transform;
		if (this.gameObject.name == "Player1") {
			Move (PlayerInput.H, PlayerInput.V);
			Animating (PlayerInput.H, PlayerInput.V);
		}
		if (this.gameObject.name == "Player2") {
			Move (PlayerInput.H2, PlayerInput.V2);
			Animating (PlayerInput.H2, PlayerInput.V2);
		}
	}

	void Move(float h, float v)	
	{
		playerRigidBody.position += transform.forward * Time.deltaTime * speed * v;
		if (v < 0) {
			playerRigidBody.transform.Rotate (0,Time.deltaTime*rotationSpeed*h*-1,0);
		} else {
			playerRigidBody.transform.Rotate (0,Time.deltaTime*rotationSpeed*h,0);
		}
	}

	void Animating(float h, float v)
	{
		bool walking = h != 0f || v != 0f;

		anim.SetBool ("isWalking", walking);
	}
}
