﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;


public class PlayerHealth : MonoBehaviour
{
	public int startingHealth = 100;
	public int currentHealth;
	public bool isDead;

    public Slider healthSlider;
    public Image damageImage;
    public AudioClip deathClip;
    public float flashSpeed = 5f;
    public Color flashColour = new Color(1f, 0f, 0f, 0.1f);

    Animator anim;
    AudioSource playerAudio;
    PlayerMovement playerMovement;
    PlayerShooting playerShooting;

	int deathcounter = 50;
	int counter;
    bool damaged;
	private float intensity = 0.1f;
	private float flashIntensity = 0f;
	private int origStartHealth = 100;

    void Awake ()
    {
		anim = GetComponentInChildren<Animator> ();
        playerAudio = GetComponent <AudioSource> ();
        playerMovement = GetComponent <PlayerMovement> ();
        playerShooting = GetComponentInChildren <PlayerShooting> ();
		startingHealth = origStartHealth;
		currentHealth = startingHealth;
		isDead = false;
		counter = 0;
    }
		
    void Update ()
    {
		if (isDead) {
			counter += 1;
			if (counter >= deathcounter) {
				anim.SetBool ("Die", false);
				counter = -500000000;
			}
		}

        if(damaged)
        {
			intensity = 1 - (currentHealth / startingHealth);
			flashColour = new Color(1f, 0f, 0f, intensity);
			flashIntensity = intensity * flashSpeed;
            damageImage.color = flashColour;
        }
        else
        {
			damageImage.color = Color.Lerp (damageImage.color, Color.clear, flashIntensity * Time.deltaTime);
        }
        damaged = false;
		healthSlider.value = ((float)currentHealth / (float)startingHealth)*100f;
    }

	public void UpdateHealth(int value){
		currentHealth += value;
		if (currentHealth > startingHealth) {
			currentHealth = startingHealth;
		}
	}

    public void TakeDamage (int amount)
    {
        damaged = true;

        currentHealth -= amount;
		healthSlider.value = ((float)currentHealth / (float)startingHealth)*100f;

        playerAudio.Play ();

        if(currentHealth <= 0 && !isDead)
        {
			isDead = true;
            Death ();
        }
    }

    void Death ()
    {
        

        playerShooting.DisableEffects ();

        anim.SetTrigger ("Die");

        playerAudio.clip = deathClip;
        playerAudio.Play ();

        playerMovement.enabled = false;
        playerShooting.enabled = false;
		damageImage.color = Color.clear;
		//GameOverManager.GameOver = true;
    }
}
