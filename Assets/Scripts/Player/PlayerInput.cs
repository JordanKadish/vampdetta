﻿using UnityEngine;
using System.Collections;

public class PlayerInput : MonoBehaviour {

	public static float H = 0f;
	public static float V = 0f;
	public static float H2 = 0f;
	public static float V2 = 0f;

	public static KeyCode LEFT_TARGET = KeyCode.I;
	public static KeyCode RIGHT_TARGET = KeyCode.P;
	public static KeyCode FIRE_KEY = KeyCode.O;

	public static KeyCode LEFT_TARGET2 = KeyCode.Q;
	public static KeyCode RIGHT_TARGET2 = KeyCode.E;
	public static KeyCode FIRE_KEY2 = KeyCode.F;

	// Update is called once per frame
	void FixedUpdate () {
		H = Input.GetAxisRaw ("Horizontal1");
		V = Input.GetAxisRaw ("Vertical1");
		H2 = Input.GetAxisRaw ("Horizontal2");
		V2 = Input.GetAxisRaw ("Vertical2");
	}
}
	