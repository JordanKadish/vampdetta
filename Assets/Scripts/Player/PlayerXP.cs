﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerXP : MonoBehaviour {
	public float currentXP;
	public int level;
	private float nextLevel;
	private float oldLevel;
	public Slider XPSlider;
	public float multiplier = 1.1f;
	private float sliderValue;
	private float sliderMax;
	public float initialLevelUp = 50f;
	public ParticleSystem levelParticles;

	PlayerHealth playerHealth;
	PlayerMovement playerMovement;
	PlayerShooting playerShooting;

	public int score;
	public int gold;
	public Text textScore;
	public Text textLevel;

	void Awake () {
		score = 0;
		currentXP = 0f;
		level = 1;
		sliderValue = 0f;
		nextLevel = initialLevelUp;
		sliderMax = nextLevel;
		playerHealth = GetComponent <PlayerHealth> ();
		playerMovement = GetComponent <PlayerMovement> ();
		playerShooting = GetComponent <PlayerShooting> ();
	}

	public void levelUp(){
		oldLevel = nextLevel;
		sliderValue -= oldLevel;
		nextLevel *= (multiplier*multiplier*multiplier*multiplier*multiplier);
		sliderMax = nextLevel - oldLevel;
		level++;

		ParticleSystem particles = (ParticleSystem)Instantiate (levelParticles, this.transform.position, this.transform.rotation);
		particles.GetComponent<ParticleDeath> ().followPlayer = true;

		playerShooting.damagePerShot = (int)((float)playerShooting.damagePerShot*multiplier);
		playerShooting.range += multiplier * 5;
		playerShooting.gunLightIntensity *= multiplier;
		playerShooting.timeBetweenBullets /= multiplier;

		playerHealth.startingHealth += (int)(10 * multiplier);
		playerHealth.UpdateHealth(playerHealth.startingHealth/2);
		EnemyManager.spawnTime /= multiplier;
		playerMovement.speed *= multiplier;
	}

	// Update is called once per frame
	void Update () {
		currentXP = score;

		sliderValue = currentXP - oldLevel;
		XPSlider.value = (sliderValue / sliderMax)*100f;
		if (currentXP >= nextLevel) {
			levelUp ();
		}
		textScore.text = "SCORE : " + score;
		textLevel.text = ""+level;
	}
}
