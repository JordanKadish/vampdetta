﻿using UnityEngine;
using System.Collections;

public class RandomManager : MonoBehaviour {
	private float timer = 0f;
	public float spawnTime = 30f;
	public float max = 25f;
	public float min = -25f;
	public float y_floor = -0.2f;

	public static float PowerBoostChance = 30f;
	private float RandomChance;

	public GameObject[] spawnObjects;
	public Transform[] spawnLocations;
	public Texture[] textures;

	// Use this for initialization
	void Start () {
		InvokeRepeating ("spawnObject", spawnTime, spawnTime);
	}

	void spawnObject(){
		//int spawn_no = Random.Range (0, spawnLocations.Length);
		int obj_no = Random.Range (0, spawnObjects.Length);
		int text_no = Random.Range (0, textures.Length);
		float x = Random.Range (min, max);
		float z = Random.Range (min, max);

		RandomChance = Random.Range (0f, 100f);


		GameObject obj = (GameObject)Instantiate (spawnObjects [obj_no], new Vector3(x,y_floor,z), Random.rotation);
		if (obj.gameObject.tag == "AddTexture") {
			obj.GetComponent<MeshRenderer> ().material.mainTexture = textures [text_no];  
		}
		if (RandomChance < PowerBoostChance) {
			obj.GetComponent<RandomObjectScript> ().PowerBoost = TempPowerBoosts.getPowerBoost ();
		}  
	}
}