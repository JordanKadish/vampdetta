﻿using UnityEngine;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour
{
	public Image Aim;
	public float restartDelay = 30f;
	public float inputDelay = 5f;
	public Text gameOverText;
	public float introDelay = 10f;
	public GameObject text3D;
	public Animator anim;

	float restartTimer;
	public static bool GameOver;

	private string Message = "GAME OVER \nRESTART in .. ";
	private string MessageUpdate = "\nPress Any Button";

	private bool timerStarted = false;

	void Awake(){
		GameOver = false;
		timerStarted = false;
		introDelay += Time.deltaTime;
		text3D.SetActive (true);
    }
		
    void Update()
    {
		restartTimer += Time.deltaTime;
		if (restartTimer > introDelay) {
			text3D.SetActive (false);
		}
		if (GameOver) {
			Aim.gameObject.SetActive (false);
			anim.SetTrigger ("GameOver");
			gameOverText.color = new Color (1, 0, 0, 1);

			if (!timerStarted) {
				timerStarted = true;
				restartDelay += restartTimer;
				inputDelay += restartTimer;
			}
			if (restartTimer >= inputDelay) {
				gameOverText.text = Message + (int)(restartDelay - restartTimer) + MessageUpdate;
			} else {
				gameOverText.text = Message + (int)(restartDelay - restartTimer);
			}
			if ((restartTimer >= inputDelay && Input.anyKeyDown) || restartTimer >= restartDelay) {
				GameOver = false;
				Application.LoadLevel (Application.loadedLevel);
				//Application.LoadLevelAsync(Application.loadedLevelName);
			}
		} else {
			gameOverText.color = new Color (1, 0, 0, 0);
		}
    }
}
