﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class EnemyHealth : MonoBehaviour
{
    public int startingHealth = 100;
    public int currentHealth;
    public float sinkSpeed = 2.5f;
    public int scoreValue = 10;
    public AudioClip deathClip;
	public int goldValue = 10;

    Animator anim;
    AudioSource enemyAudio;
    ParticleSystem hitParticles;
    CapsuleCollider capsuleCollider;
	EnemyMovement enemyMovement;
	GameObject[] players;

    bool isDead;
    bool isSinking;

    void Awake ()
    {
		players = GameObject.FindGameObjectsWithTag ("Player");
		anim = GetComponentInChildren<Animator> ();
        enemyAudio = GetComponent <AudioSource> ();
        hitParticles = GetComponentInChildren <ParticleSystem> ();
        capsuleCollider = GetComponent <CapsuleCollider> ();
		enemyMovement = GetComponent<EnemyMovement> ();
        currentHealth = startingHealth;
    }
		
    void Update ()
    {
		if(isSinking)
        {
            transform.Translate (-Vector3.up * sinkSpeed * Time.deltaTime);
        }
    }
		
	public void TakeDamage (int amount, Vector3 hitPoint, GameObject player)
    {
        if(isDead)
            return;

        enemyAudio.Play ();

        currentHealth -= amount;
            
        hitParticles.transform.position = hitPoint;
        hitParticles.Play();

        if(currentHealth <= 0)
        {
            Death (player);
        }
    }

	void Death (GameObject player)
    {
		for (int i = 0; i < players.Length; i++) {
			players [i].GetComponentInChildren<PlayerAutoAim> ().removeTarget(this.gameObject);
		}

        isDead = true;

		EnemyManager.EnemyDeath ();

        capsuleCollider.isTrigger = true;

        anim.SetTrigger ("Die");
		player.GetComponent<PlayerXP>().score += scoreValue;
        enemyAudio.clip = deathClip;
        enemyAudio.Play ();
		StartSinking ();
    }
		
    public void StartSinking ()
    {
        GetComponent <NavMeshAgent> ().enabled = false;
        GetComponent <Rigidbody> ().isKinematic = true;
        isSinking = true;

        Destroy (gameObject, 2f);
    }
}
