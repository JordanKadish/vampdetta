﻿using UnityEngine;
using System.Collections;

public class RandomObjectScript : MonoBehaviour {
	public int currentHealth;
	public int startingHealth = 5;
	private bool isDead;
	public float sinkSpeed = 2f;
	public ParticleSystem hitParticles;
	public ParticleSystem deathParticles;
	private float RotateSpeed;
	private int goldValue;
	public bool followPlayer;
	public int PowerBoost = 0;

	// Use this for initialization
	void Start () {
		currentHealth = startingHealth;
		RotateSpeed = Random.Range (5f, 20f);
		goldValue = Random.Range (10,50);
		//goldValue = Random.Range (0, PlayerXP.score/10);
	}

	public void TakeDamage (int amount, Vector3 hitPoint, GameObject player)
	{
		if(isDead)
			return;

		currentHealth -= amount;
		hitParticles.transform.position = hitPoint;
		hitParticles.Play();

		if(currentHealth <= 0)
		{
			DestroyObject (player);
		}
	}

	public void DestroyObject(GameObject player){
		isDead = true;
		if (PowerBoost != 0) {
			TempPowerBoosts.setPowerBoost (PowerBoost);
		}
		ParticleSystem particles = (ParticleSystem)Instantiate (deathParticles, PlayerMovement.playerPosition.position, PlayerMovement.playerPosition.rotation);
		particles.GetComponent<ParticleDeath> ().followPlayer = followPlayer;
		player.GetComponent<PlayerXP>().score += goldValue;
		Destroy (this.gameObject, 2f);
	}

	// Update is called once per frame
	void Update () {
		if (isDead) {
			transform.Translate (-Vector3.up * sinkSpeed * Time.deltaTime);
			isDead = true;
		} else {
			transform.Rotate (new Vector3 (RotateSpeed, RotateSpeed * 2, RotateSpeed * 3) * Time.deltaTime);
		}
	}
}