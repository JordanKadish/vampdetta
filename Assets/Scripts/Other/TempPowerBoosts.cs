﻿using UnityEngine;
using System.Collections;

public class TempPowerBoosts : MonoBehaviour {
	public static int totalPowerBoosts = 2;
	public ParticleSystem[] particleEffects;

	public static int newBoostValue = 0;
	private int ActiveBoostValue = 0;
	private bool isActive = false;
	private bool ActiveStarted;
	public static bool newBoostAvail;
	GameObject[] players;
	GameObject player1;
	GameObject player2;

	private float Activetime;
	public static float BoostTime = 20f;
	ParticleSystem ActiveParticles;

	private float timer = 0f;

	// Use this for initialization
	void Start () {
		players = GameObject.FindGameObjectsWithTag ("Player");
		for (int i = 0; i < players.Length; i++) {
			if (players [i].name == "Player1") {
				player1 = players [i];
			}
			if (players [i].name == "Player2") {
				player2 = players [i];
			}
		}
		timer = 0f;
	}

	void resetAll(){
		isActive = false;
		ActiveStarted = false;
		BoostTime = 20f;
	}

	public static int getPowerBoost(){
		return Random.Range (1,totalPowerBoosts);
	}

	public static void setPowerBoost(int value){
		newBoostValue = value;
		newBoostAvail = true;
	}

	void startBoost(){
		ActiveParticles = (ParticleSystem)Instantiate (particleEffects[0], player1.transform.position, player1.transform.rotation);
		Activetime = BoostTime + timer;
		if (newBoostAvail) {//update new to active
			ActiveBoostValue = newBoostValue;
			newBoostAvail = false;
		}
		ActiveParticles.GetComponent<ParticleDeath> ().DeathTime = BoostTime;
		UsePowerBoost (ActiveBoostValue);
		ActiveStarted = true;
	}

	void stopBoost(){
		ActiveParticles.GetComponent<ParticleDeath> ().DestroyObject ();
		ResetPowerBoost (ActiveBoostValue);

		if (newBoostAvail) {//update new to active
			ActiveBoostValue = newBoostValue;
			newBoostAvail = false;
		}

		resetAll ();
	}

	void ResetPowerBoost(int value){
		switch (value) {
		case 1:
			player1.GetComponent<PlayerShooting>().damagePerShot /= 2;
			break;

		case 2:
			player1.GetComponent<PlayerShooting>().timeBetweenBullets *= 2;
			break;

		default:
			break;
		}
	}


	public void UsePowerBoost(int value){
		switch (value) {
		case 1://Double Damage
			player1.GetComponent<PlayerShooting>().damagePerShot *= 2; // possible relocate
			break;

		case 2:
			player1.GetComponent<PlayerShooting>().timeBetweenBullets /= 2;
			break;

		default:
			break;
		}
	}

	// Update is called once per frame
	void Update () {
//		if (BoostActivate) {
//			UsePowerBoost ();
		//		}
		if (newBoostAvail && ActiveStarted && isActive && timer >= Activetime) {
			stopBoost ();
			startBoost ();
		}
		if (!newBoostAvail && ActiveStarted && isActive && timer >= Activetime) {
			stopBoost ();
		}
		if (newBoostAvail && !ActiveStarted && !isActive) {
			startBoost ();
		}

		timer += Time.deltaTime;
		//if (DDActive && timer > BoostTimer) {
		//	PlayerShooting.damagePerShot /= 2;
		//}

		//resetAll ();
	}
}
