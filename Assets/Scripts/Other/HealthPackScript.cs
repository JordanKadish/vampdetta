﻿using UnityEngine;
using System.Collections;

public class HealthPackScript : MonoBehaviour {
	private int health;
	RandomObjectScript objectScript;

	// Use this for initialization
	void Start () {
		health = Random.Range (20,100);
		objectScript = GetComponent<RandomObjectScript> ();
	}

	void OnTriggerEnter (Collider collider){
		PlayerHealth temp = collider.gameObject.GetComponentInParent<PlayerHealth> ();
		if (temp != null) {
			if (collider.GetType () == typeof(CapsuleCollider)) {
				temp.UpdateHealth (health);
				objectScript.DestroyObject (temp.gameObject);
			}
		}
	}
}
